#ifndef XCPROJ_TOKENISER_H
#define XCPROJ_TOKENISER_H

#if defined(__cplusplus)
#include <cstdint>
#else
#include <stdint.h>
#endif

#if defined(__cplusplus)
extern "C" {
#endif

// Constants
typedef enum xcproj_token_type {
	xcproj_token_type_none,
	xcproj_token_type_open_brace,
	xcproj_token_type_close_brace,
	xcproj_token_type_open_parenthesis,
	xcproj_token_type_close_parenthesis,
	xcproj_token_type_comma,
	xcproj_token_type_equal,
	xcproj_token_type_terminator,
	xcproj_token_type_string,
	xcproj_token_type_unknown,
} xcproj_token_type_t;

typedef enum xcproj_token_tag {
	xcproj_token_tag_string_is_literal = (1 << 0)
} xcproj_token_tag_t;

// Types
typedef struct xcproj_tokeniser	xcproj_tokeniser_t;

typedef struct xcproj_token {
	xcproj_token_type_t	type;
	uint32_t			tags;
	char const * 		string;
} xcproj_token_t;

// Functions
xcproj_tokeniser_t * 	xcproj_tokeniser_create(void);
void					xcproj_tokeniser_destroy(xcproj_tokeniser_t * tokeniser);

xcproj_token_t			xcproj_tokeniser_next_token(xcproj_tokeniser_t * tokeniser, char const ** chunk_data, size_t * chunk_size);

#if defined(__cplusplus)
}
#endif

#endif	// XCPROJ_TOKENISER_H
