#ifndef XCPROJ_PARSER_H
#define XCPROJ_PARSER_H

#include "xcproj/parse/tokeniser.h"

#if defined(__cplusplus)
extern "C" {
#endif

// Constants
typedef enum xcproj_parser_error {
	xcproj_parser_error_none,
	xcproj_parser_error_invalid_token,
	xcproj_parser_error_malformed
} xcproj_parser_error_t;

// Callback types
typedef void (*xcproj_parser_begin_object_definition_callback_t)(void * user_context);
typedef void (*xcproj_parser_end_object_definition_callback_t)(void * user_context);
typedef void (*xcproj_parser_begin_array_definition_callback_t)(void * user_context);
typedef void (*xcproj_parser_end_array_definition_callback_t)(void * user_context);
typedef void (*xcproj_parser_begin_assignment_callback_t)(void * user_context, char const * identifier);
typedef void (*xcproj_parser_end_assignment_callback_t)(void * user_context);
typedef void (*xcproj_parser_string_constant_callback_t)(void * user_context, char const * string);

// Types
typedef struct xcproj_parser_callback_container {
	xcproj_parser_begin_object_definition_callback_t	begin_object_definition;
	xcproj_parser_end_object_definition_callback_t		end_object_definition;
	xcproj_parser_begin_array_definition_callback_t		begin_array_definition;
	xcproj_parser_end_array_definition_callback_t		end_array_definition;
	xcproj_parser_begin_assignment_callback_t			begin_assignment;
	xcproj_parser_end_assignment_callback_t				end_assignment;
	xcproj_parser_string_constant_callback_t			string_constant;
} xcproj_parser_callback_container_t;

typedef struct xcproj_parser xcproj_parser_t;

// Functions
char const *			xcproj_parser_error_get_description(xcproj_parser_error_t const error);

xcproj_parser_t *		xcproj_parser_create(xcproj_parser_callback_container_t const user_callbacks);
void					xcproj_parser_destroy(xcproj_parser_t * parser);

xcproj_parser_error_t	xcproj_parser_parse_token(xcproj_parser_t * parser, xcproj_token_t * token, void * user_context);

#if defined(__cplusplus)
}
#endif

#endif	// XCPROJ_PARSER_H
