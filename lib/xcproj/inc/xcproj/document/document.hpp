#ifndef XCPROJ_DOCUMENT_HPP
#define XCPROJ_DOCUMENT_HPP

#if !defined(__cplusplus)
#error "this ain't gonna work for you pal"
#endif

// Includes
#include <string>

#include <iostream>
#include <vector>
#include <unordered_map>

#include "xcproj/parse/tokeniser.h"
#include "xcproj/parse/parser.h"

#include "xcproj/types/variant.hpp"

namespace xcproj {

	class document {
	public:
		// Forward declarations
		struct node;

		struct array_node {
			std::vector<document::node *> elements;
		};

		struct obj_node {
			std::unordered_map<std::string, document::node *> pairs;
		};

		struct node {
			variant<std::string, document::array_node, document::obj_node> value;
		};

	public:
		node * base_node;

	public:
		document(void) : base_node(nullptr) {}
		document(std::istream & input_stream) : base_node(nullptr) { from_stream(input_stream); }

		~document(void) {
			for (auto node : m_free_node_pool) {
				delete node;
			}
		}

		// Populate the document object by parsing the input in the given stream
		bool from_stream(std::istream & input_stream) {
			parser stream_parser(*this);
			return stream_parser.parse_stream(input_stream);
		}

		// Write formatted output for the document object to the given stream
		bool to_stream(std::ostream & output_stream) {
			writer stream_writer(*this);
			return stream_writer.to_stream(output_stream);
		}

		// FIXME: This kind of allocation is stupid, and I'm not freeing anything
		node * allocate_node(void) {
			if (0 == m_free_node_pool.size()) {
				m_free_node_pool.push_back(new node());
			}

			node * new_node = m_free_node_pool.back();
			m_free_node_pool.pop_back();
			return new_node;
		}

		void free_node(node * node_ptr) {
			m_free_node_pool.push_back(node_ptr);
		}

		class parser {
		public:
			parser(document & target) : m_target(target) {}

			bool parse_stream(std::istream & input_stream) {
				xcproj_parser_callback_container_t parser_callbacks = {
					&begin_obj_def,
					&end_obj_def,
					&begin_array_def,
					&end_array_def,
					&begin_assignment,
					&end_assignment,
					&string_constant
				};

				xcproj_tokeniser_t * tokeniser = xcproj_tokeniser_create();
				xcproj_parser_t * parser = xcproj_parser_create(parser_callbacks);

				constexpr size_t c_input_block_size = 2048;
				do {
					char buffer[c_input_block_size];
					char const * buffer_ptr = buffer;
					input_stream.read(buffer, c_input_block_size);
					size_t read_count = input_stream.gcount();

					while (read_count > 0) {
						xcproj_token_t token = xcproj_tokeniser_next_token(tokeniser, &buffer_ptr, &read_count);

						// This means the end of input data
						if (xcproj_token_type_none == token.type)
							break;

						xcproj_parser_error_t result = xcproj_parser_parse_token(parser, &token, static_cast<void *>(this));

						if (xcproj_parser_error_none != result)
						{
							m_error = true;
							break;
						}
					}
				}
				while (input_stream.good());

				xcproj_parser_destroy(parser);
				xcproj_tokeniser_destroy(tokeniser);

				return (!m_error);
			}
			
		private:
			// Parser callbacks
			static void begin_obj_def(void * user_context) {
				parser * context = static_cast<parser *>(user_context);

				// Allocate a new node of type object and put it onto the stack for populating
				document::node * new_node = context->m_target.allocate_node();
				new_node->value.set<obj_node>();
				context->m_node_stack.push_back(new_node);
			}

			static void end_obj_def(void * user_context) {
				parser * context = static_cast<parser *>(user_context);

				document::node * obj_node = context->m_node_stack.back();

				// Check if there is anything else left on the stack, if not, we're done
				if (1 == context->m_node_stack.size()) {
					context->m_target.base_node = context->m_node_stack.back();
					context->m_node_stack.pop_back();
				}
				else if (context->m_node_stack.size() > 2) {
					// Add it to the parent array if necessary
					document::node * parent_node = *(context->m_node_stack.end() - 2);

					if (parent_node->value.is<document::array_node>()) {
						document::array_node & array_node = parent_node->value.get<document::array_node>();
						array_node.elements.push_back(obj_node);
						context->m_node_stack.pop_back();
					}

					// Otherwise, leave it on the stack for further processing
				}
			}

			static void begin_array_def(void * user_context) {
				parser * context = static_cast<parser *>(user_context);

				// Allocate a new node of type array and put it onto the stack for populating
				document::node * new_node = context->m_target.allocate_node();
				new_node->value.set<array_node>();
				context->m_node_stack.push_back(new_node);
			}

			static void end_array_def(void * user_context) {
				parser * context = static_cast<parser *>(user_context);

				document::node * array_node = context->m_node_stack.back();
				document::node * parent_node = *(context->m_node_stack.end() - 2);

				// Check for a parent array and add it if necessary.
				// NOTE: Not seen, but assuming this would work the same way.
				// Recursive arrays like this seem unlikely for a whole host
				// of reasons
				if (parent_node->value.is<document::array_node>()) {
					document::array_node & parent_array_node = parent_node->value.get<document::array_node>();
					parent_array_node.elements.push_back(array_node);
					context->m_node_stack.pop_back();
				}

				// Otherwise, leave this on the stack for further processing
			}

			static void begin_assignment(void * user_context, char const * identifier) {
				parser * context = static_cast<parser *>(user_context);

				// Store the identifier on the stack as a string so that when the 
				// assignment is complete it can be popped off and used
				document::node * new_node = context->m_target.allocate_node();
				new_node->value.set<std::string>(identifier);
				context->m_node_stack.push_back(new_node);
			}

			static void end_assignment(void * user_context) {
				parser * context = static_cast<parser *>(user_context);

				// Left on the stack by whatever
				document::node * assignment_node = context->m_node_stack.back();
				context->m_node_stack.pop_back();
				
				// Left on the stack by begin_assignment callback
				document::node * identifier_node = context->m_node_stack.back();
				context->m_node_stack.pop_back();

				document::node * obj_node = context->m_node_stack.back();

				if (identifier_node->value.is<std::string>() &&
					obj_node->value.is<document::obj_node>())
				{
					document::obj_node & obj = obj_node->value.get<document::obj_node>();
					obj.pairs.emplace(identifier_node->value.get<std::string>(), assignment_node);

					// Don't need the identifer's node anymore, it was temporary storage
					context->m_target.free_node(identifier_node);
				}
				else {
					fprintf(stderr, "ERROR, ended assignment and did not find the correct nodes on the stack\n");
					context->m_error = true;
				}
			}

			static void string_constant(void * user_context, char const * string) {
				parser * context = static_cast<parser *>(user_context);

				// Allocate a new node with the integer in it and put it onto the stack
				// ready for assignment to wherever it should go
				document::node * new_node = context->m_target.allocate_node();
				new_node->value.set<std::string>(string);

				document::node * parent_node = context->m_node_stack.back();
				if (parent_node->value.is<document::array_node>()) {
					// Add it to the array
					document::array_node & array_node = parent_node->value.get<document::array_node>();
					array_node.elements.push_back(new_node);
				}
				else {
					// Put it on the stack for further processing
					context->m_node_stack.push_back(new_node);
				}
			}

			document & m_target;
			
			std::vector<node *> m_node_stack;

			bool m_error;
		};

		class writer {
		public:
			writer(document & target) : m_target(target), m_curr_indent(0) {}

			bool to_stream(std::ostream & output_stream) {
				output_stream << "// !$*UTF8*$!\n";
				bool result = to_stream(output_stream, m_target.base_node);
				output_stream << std::endl;
				return result;
			}

			bool to_stream(std::ostream & output_stream, node * node) {
				if (nullptr == node)
					return true;

				if (node->value.is<obj_node>()) {
					return to_stream(output_stream, node->value.get<obj_node>());
				}
				else if (node->value.is<array_node>()) {
					return to_stream(output_stream, node->value.get<array_node>());
				}
				else if (node->value.is<std::string>()) {
					return to_stream(output_stream, node->value.get<std::string>());
				}
				else {
					fprintf(stderr, "Unhandled node type in to_stream\n");
				}
				
				return false;
			}

			bool to_stream(std::ostream & output_stream, obj_node & node) {
				bool result = true;

				output_stream << "{\n";
				m_curr_indent++;

				for (auto pair = node.pairs.cbegin();
						pair != node.pairs.cend() && result;
						++pair) {
					indent(output_stream, m_curr_indent);
					output_stream << pair->first << " = ";
					result = to_stream(output_stream, pair->second);
					output_stream << ";\n";
				}

				m_curr_indent--;
				indent(output_stream, m_curr_indent);
				output_stream << "}";

				return result;
			}

			bool to_stream(std::ostream & output_stream, array_node & node) {
				bool result = true;

				output_stream << "(\n";
				m_curr_indent++;

				for (auto element = node.elements.cbegin();
						element != node.elements.cend() && result;
						++element) {
					indent(output_stream, m_curr_indent);
					result = to_stream(output_stream, *element);
					output_stream << ",\n";
				}

				m_curr_indent--;
				indent(output_stream, m_curr_indent);
				output_stream << ")";
				
				return result;
			}

			bool to_stream(std::ostream & output_stream, std::string & node) {
				// Determine if we need to quote this string, it will already be suitably
				// escaped

				auto iter = node.find_first_not_of("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789./_");

				// Empty string should be quoted also, Xcode fails to open projects without this quoted
				if (node.empty() || iter != std::string::npos) {
					// Quote the string
					output_stream << '"' << node << '"';
				}
				else {
					output_stream << node;
				}
				
				return true;
			}

		private:
			static void indent(std::ostream & output_stream, size_t num) {
				while (num --> 0) {
					output_stream.put('\t');
				}
			}

			document & m_target;

			size_t m_curr_indent;
		};
		
	private:
		std::vector<node *> m_free_node_pool;
	};

}

#endif // XCPROJ_DOCUMENT_HPP

