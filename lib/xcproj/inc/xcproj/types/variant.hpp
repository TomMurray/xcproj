#ifndef XCPROJ_VARIANT_HPP
#define XCPROJ_VARIANT_HPP

// Includes
#include <functional>
#include <type_traits>
#include <typeinfo>

namespace {
	// Helpers

	// Compile time maximum of a given range of size_t values
	template <size_t v, size_t ...values>
	struct max_of_t;

	template <size_t v>
	struct max_of_t<v> {
		constexpr static size_t value = v;
	};

	template <size_t v1, size_t v2, size_t ...remaining>
	struct max_of_t<v1, v2, remaining...> {
		constexpr static size_t value = v1 > v2 ? max_of_t<v1, remaining...>::value : max_of_t<v2, remaining...>::value;
	};

}

namespace xcproj {

	template <typename... Ts>
	class variant {
	private:
		constexpr static size_t c_max_size	= max_of_t<sizeof(Ts)...>::value;
		constexpr static size_t c_max_align	= max_of_t<alignof(Ts)...>::value;

		using storage_t = typename std::aligned_storage<c_max_size, c_max_align>::type;

	public:
		variant(void) : m_type_hash(typeid(void).hash_code()) {}
		~variant(void) {
			if (m_destructor)
				m_destructor(&m_storage);

			m_type_hash = typeid(void).hash_code();
		}

		template <typename T, typename... Args>
		void set(Args&&... args) {
			// Destruct old value if needed
			if (m_destructor)
				m_destructor(&m_storage);

			// Setup the new value, type hash, and destructor for that type
			// as needed
			new (&m_storage) T(std::forward<Args>(args)...);
			m_type_hash = typeid(T).hash_code();
			m_destructor = [](storage_t * ptr) {
				(reinterpret_cast<T*>(ptr))->~T();
			};
		}

		template <typename T>
		bool is(void) {
			return (m_type_hash == typeid(T).hash_code());
		}

		template <typename T>
		T& get(void) {
			return *reinterpret_cast<T*>(&m_storage);
		}

	private:
		std::function<void(storage_t*)> m_destructor;
		storage_t m_storage;
		size_t m_type_hash;
	};

}

#endif	// XCPROJ_VARIANT_HPP

