#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "xcproj/parse/parser.h"

// For token definitions
#include "xcproj/parse/tokeniser.h"

// Private definitions
#define MAX_STATE_STACK	2048

#define PARSE_CALLBACK(_callback_, ...)	if (_callback_) { _callback_(__VA_ARGS__); }
#define PARSER_STATE(_state_)			xcproj_parser_state_##_state_
#define PARSER_ERROR(_error_)			xcproj_parser_error_##_error_
#define TOKEN(_token_)					xcproj_token_type_##_token_

// Private constants
typedef enum xcproj_parser_state {
	xcproj_parser_state_none,
	xcproj_parser_state_definition,
	xcproj_parser_state_object_definition,
	xcproj_parser_state_array_definition,
	xcproj_parser_state_array_more,
	xcproj_parser_state_begin_assignment,
	xcproj_parser_state_finish_assignment,
} xcproj_parser_state_t;

// Private types
struct state_stack {
	size_t top;
	xcproj_parser_state_t stack[MAX_STATE_STACK];
};	

struct xcproj_parser {
	xcproj_parser_callback_container_t callbacks;
	struct state_stack	state_stack;
};


// Private API
static inline void state_stack_push(struct state_stack * state_stack, xcproj_parser_state_t state) {
	assert(state_stack->top < MAX_STATE_STACK);

	state_stack->stack[++state_stack->top] = state;
}

static inline xcproj_parser_state_t const state_stack_pop(struct state_stack * state_stack) {
	assert(state_stack->top > 0);

	xcproj_parser_state_t const top = state_stack->stack[state_stack->top];
	state_stack->top--;
	return top;
}

static inline xcproj_parser_state_t const state_stack_top(struct state_stack * state_stack) {
	return state_stack->stack[state_stack->top];
}


// Public API
char const * xcproj_parser_error_get_description(xcproj_parser_error_t const error) {
#define DEFINE_ERROR_DESC_CASE(err, desc) case xcproj_parser_error_##err: return desc; break;
	switch (error) {
		DEFINE_ERROR_DESC_CASE(none, 			"No error");
		DEFINE_ERROR_DESC_CASE(invalid_token, 	"Invalid token");
		DEFINE_ERROR_DESC_CASE(malformed,		"Malformed input");
	}
#undef DEFINE_ERROR_DESC_CASE

	return NULL;
}

xcproj_parser_t * xcproj_parser_create(xcproj_parser_callback_container_t const user_callbacks) {
	xcproj_parser_t * new_parser = (xcproj_parser_t *)malloc(sizeof(xcproj_parser_t));
	memset(new_parser, 0x0, sizeof(xcproj_parser_t));

	new_parser->callbacks = user_callbacks;

	return new_parser;
}

void xcproj_parser_destroy(xcproj_parser_t * parser) {
	free(parser);
}


xcproj_parser_error_t xcproj_parser_parse_token(xcproj_parser_t * parser, xcproj_token_t * token, void * user_context) {
	xcproj_parser_error_t result = PARSER_ERROR(none);

	xcproj_parser_state_t curr_state = state_stack_top(&parser->state_stack);
	switch (curr_state) {
		case PARSER_STATE(none): {
			switch (token->type) {
				case TOKEN(open_brace): {
					state_stack_push(&parser->state_stack, PARSER_STATE(object_definition));
					PARSE_CALLBACK(parser->callbacks.begin_object_definition, user_context);
					break;
				}
				default: {
					result = PARSER_ERROR(malformed);
					break;
				}
			}
			break;
		}
		case PARSER_STATE(definition): {
			switch (token->type) {
				case TOKEN(string): {
					state_stack_pop(&parser->state_stack);
					PARSE_CALLBACK(parser->callbacks.string_constant, user_context, token->string);
					break;
				}
				case TOKEN(open_brace): {
					state_stack_pop(&parser->state_stack);
					state_stack_push(&parser->state_stack, PARSER_STATE(object_definition));
					PARSE_CALLBACK(parser->callbacks.begin_object_definition, user_context);
					break;
				}
				case TOKEN(open_parenthesis): {
					state_stack_pop(&parser->state_stack);
					state_stack_push(&parser->state_stack, PARSER_STATE(array_definition));
					PARSE_CALLBACK(parser->callbacks.begin_array_definition, user_context);
					break;
				}
				default: {
					result = PARSER_ERROR(malformed);
					break;
				}
			}
			break;
		}
		case PARSER_STATE(object_definition): {
			switch (token->type) {
				case TOKEN(string): {
					state_stack_push(&parser->state_stack, PARSER_STATE(begin_assignment));
					PARSE_CALLBACK(parser->callbacks.begin_assignment, user_context, token->string);
					break;
				}
				case TOKEN(close_brace): {
					state_stack_pop(&parser->state_stack);
					PARSE_CALLBACK(parser->callbacks.end_object_definition, user_context);
					break;
				}
				default: {
					result = PARSER_ERROR(malformed);
					break;
				}
			}
			break;
		}
		case PARSER_STATE(array_definition): {
			switch (token->type) {
				case TOKEN(string): {
					state_stack_push(&parser->state_stack, PARSER_STATE(array_more));
					PARSE_CALLBACK(parser->callbacks.string_constant, user_context, token->string);
					break;
				}
				case TOKEN(open_brace): {
					state_stack_push(&parser->state_stack, PARSER_STATE(array_more));
					state_stack_push(&parser->state_stack, PARSER_STATE(object_definition));
					PARSE_CALLBACK(parser->callbacks.begin_object_definition, user_context);
					break;
				}
				case TOKEN(close_parenthesis): {
					state_stack_pop(&parser->state_stack);
					PARSE_CALLBACK(parser->callbacks.end_array_definition, user_context);
					break;
				}
				default: {
					result = PARSER_ERROR(malformed);
					break;
				}
			}
			break;
		}
		case PARSER_STATE(array_more): {
			switch (token->type) {
				case TOKEN(comma): {
					state_stack_pop(&parser->state_stack);
					break;
				}
				case TOKEN(close_parenthesis): {
					state_stack_pop(&parser->state_stack);
					state_stack_pop(&parser->state_stack);
					break;
				}
				default: {
					result = PARSER_ERROR(malformed);
					break;
				}
			}
			break;
		}
		case PARSER_STATE(begin_assignment): {
			switch (token->type) {
				case TOKEN(equal): {
					state_stack_pop(&parser->state_stack);
					state_stack_push(&parser->state_stack, PARSER_STATE(finish_assignment));
					state_stack_push(&parser->state_stack, PARSER_STATE(definition));
					break;
				}
				default: {
					result = PARSER_ERROR(malformed);
					break;
				}
			}
			break;
		}
		case PARSER_STATE(finish_assignment): {
			switch (token->type) {
				case TOKEN(terminator): {
					state_stack_pop(&parser->state_stack);
					PARSE_CALLBACK(parser->callbacks.end_assignment, user_context);
					break;
				}
				default: {
					result = PARSER_ERROR(malformed);
					break;
				}
			}
			break;
		}
	}

	return result;
}

