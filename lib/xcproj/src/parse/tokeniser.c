#include <assert.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "xcproj/parse/tokeniser.h"

// Private definitions
#define MAX_STATE_STACK	128
#define MAX_STRING_LEN	4096

#define TOKENISER_STATE(_state_)	xcproj_tokeniser_state_##_state_
#define TOKEN(_token_)				xcproj_token_type_##_token_

// Private constants
typedef enum xcproj_tokeniser_state {
	xcproj_tokeniser_state_none,
	xcproj_tokeniser_state_string,
	xcproj_tokeniser_state_quoted_string_body,
	xcproj_tokeniser_state_quoted_string_escape,
	xcproj_tokeniser_state_quoted_string_end,
	xcproj_tokeniser_state_comment_begin,
	xcproj_tokeniser_state_single_line_comment,
	xcproj_tokeniser_state_multi_line_comment,
	xcproj_tokeniser_state_multi_line_comment_end,
} xcproj_tokeniser_state_t;

// Private types
struct string_pool {
	struct string_pool * next;
	char * current;
	char * end;
	char * mem;
};

struct state_stack {
	size_t top;
	xcproj_tokeniser_state_t stack[MAX_STATE_STACK];
};

struct xcproj_tokeniser {
	struct state_stack state_stack;
	char const * match_start;
	char * prev_match_storage;
	size_t prev_match_size;
	struct string_pool * string_pools;
};


// Private API
static char * mem_for_string(xcproj_tokeniser_t * tokeniser, size_t mem_required) {
	char * mem = NULL;
	struct string_pool * parent_pool = NULL;
	struct string_pool * curr_pool = tokeniser->string_pools;

	// Things will go a bit pear-shaped if we get a required length greater than the size of any pool
	assert(mem_required <= MAX_STRING_LEN);

	size_t i = 0;
	while (NULL == mem) {
		if (NULL == curr_pool) {
			// Allocate a new pool
			struct string_pool * new_pool = (struct string_pool *)malloc(sizeof(struct string_pool) + (sizeof(char) * MAX_STRING_LEN));

			// String memory is already allocated after the struct representing it
			new_pool->next = NULL;
			new_pool->mem = (char *)(new_pool + 1);
			new_pool->current = new_pool->mem;
			new_pool->end = new_pool->mem + MAX_STRING_LEN;

			if (NULL != parent_pool) {
				parent_pool->next = new_pool;
			}

			curr_pool = new_pool;
		}
	
		ptrdiff_t remaining_size = curr_pool->end - curr_pool->current;
		if (remaining_size >= mem_required) {
			mem = curr_pool->current;
			curr_pool->current += mem_required;
		}

		parent_pool = curr_pool;
		curr_pool = curr_pool->next;
	}

	return mem;
}

static inline void state_stack_push(struct state_stack * state_stack, xcproj_tokeniser_state_t state) {
	assert(state_stack->top < MAX_STATE_STACK);

	state_stack->stack[++state_stack->top] = state;
}

static inline xcproj_tokeniser_state_t const state_stack_pop(struct state_stack * state_stack) {
	assert(state_stack->top > 0);

	xcproj_tokeniser_state_t const top = state_stack->stack[state_stack->top];
	state_stack->top--;
	return top;
}

static inline xcproj_tokeniser_state_t const state_stack_top(struct state_stack * state_stack) {
	return state_stack->stack[state_stack->top];
}


// Public API
xcproj_tokeniser_t * xcproj_tokeniser_create(void) {
	xcproj_tokeniser_t * new_tokeniser = malloc(sizeof(xcproj_tokeniser_t));
	memset(new_tokeniser, 0x0, sizeof(xcproj_tokeniser_t));

	// Initialise a first string pool for the chain
	new_tokeniser->string_pools = (struct string_pool *) malloc(sizeof(struct string_pool) + (sizeof(char) * MAX_STRING_LEN));
	new_tokeniser->string_pools->next = NULL;
	new_tokeniser->string_pools->mem = (char *)(new_tokeniser->string_pools + 1);
	new_tokeniser->string_pools->current = new_tokeniser->string_pools->mem;
	new_tokeniser->string_pools->end = new_tokeniser->string_pools->mem + MAX_STRING_LEN;

	return new_tokeniser;
}

void xcproj_tokeniser_destroy(xcproj_tokeniser_t * tokeniser) {

	// Free all string pools
	struct string_pool * curr = tokeniser->string_pools;
	while (NULL != curr) {
		struct string_pool * next = curr->next;
		free(curr);
		curr = next;
	}

	free(tokeniser);
}

xcproj_token_t xcproj_tokeniser_next_token(xcproj_tokeniser_t * tokeniser, char const ** chunk_data, size_t * chunk_size) {
	xcproj_token_t token;
	memset(&token, 0x0, sizeof(token));

	char const * p 	= *chunk_data;
	char const * pe	= *chunk_data + *chunk_size;

	#define STRING_CHAR_MATCH(_c_) \
					(_c_ >= 'a' && _c_ <= 'z') || \
					(_c_ >= 'A' && _c_ <= 'Z') || \
					(_c_ >= '0' && _c_ <= '9') || \
					(_c_ == '_') || \
					(_c_ == '/') || \
					(_c_ == '.') || \
					(_c_ == '-')

	while (p < pe && TOKEN(none) == token.type) {
		xcproj_tokeniser_state_t const curr_state = state_stack_top(&tokeniser->state_stack);
		switch (curr_state) {
			case TOKENISER_STATE(none): {
				if (*p == '\n'	||
					*p == '\r'	||
					*p == ' ' 	||
					*p == '\t') {
					p++;
				}
				else if (*p == '/') {
					state_stack_push(&tokeniser->state_stack, TOKENISER_STATE(comment_begin));
					p++;
				}
				else if (STRING_CHAR_MATCH(*p)) {
					tokeniser->match_start = p;
					state_stack_push(&tokeniser->state_stack, TOKENISER_STATE(string));
				}
				else if (*p == '"') {
					tokeniser->match_start = p;
					state_stack_push(&tokeniser->state_stack, TOKENISER_STATE(quoted_string_body));
					p++;
				}
				else if (*p == '{') {
					token.type = TOKEN(open_brace);
					p++;
				}
				else if (*p == '}') {
					token.type = TOKEN(close_brace);
					p++;
				}
				else if (*p == '(') {
					token.type = TOKEN(open_parenthesis);
					p++;
				}
				else if (*p == ')') {
					token.type = TOKEN(close_parenthesis);
					p++;
				}
				else if (*p == ',') {
					token.type = TOKEN(comma);
					p++;
				}
				else if (*p == '=') {
					token.type = TOKEN(equal);
					p++;
				}
				else if (*p == ';') {
					token.type = TOKEN(terminator);
					p++;
				}
				else {
					tokeniser->match_start = p;
					token.type = TOKEN(unknown);
				}
				break;
			}
			case TOKENISER_STATE(string): {
				if (STRING_CHAR_MATCH(*p)) {
					p++;
				}
				else {
					token.type = TOKEN(string);
					state_stack_pop(&tokeniser->state_stack);
				}
				break;
			}
			case TOKENISER_STATE(quoted_string_body): {
				if (*p == '"') {
					token.type = TOKEN(string);
					token.tags |= xcproj_token_tag_string_is_literal;
					state_stack_pop(&tokeniser->state_stack);
					p++;
				}
				else if (*p == '\\') {
					state_stack_push(&tokeniser->state_stack, TOKENISER_STATE(quoted_string_escape));
					p++;
				}
				else {
					p++;
				}
				break;
			}
			case TOKENISER_STATE(quoted_string_escape): {
				if (*p == '\\' ||
					*p == 'n' ||
					*p == 'r' ||
					*p == 't' ||
					*p == '"') {
					state_stack_pop(&tokeniser->state_stack);
					p++;
				}
				else {
					token.type = TOKEN(unknown);
				}
				break;
			}
			case TOKENISER_STATE(comment_begin): {
				if (*p == '/') {
					state_stack_pop(&tokeniser->state_stack);
					state_stack_push(&tokeniser->state_stack, TOKENISER_STATE(single_line_comment));
					p++;
				}
				else if (*p == '*') {
					state_stack_pop(&tokeniser->state_stack);
					state_stack_push(&tokeniser->state_stack, TOKENISER_STATE(multi_line_comment));
					p++;
				}
				else if (STRING_CHAR_MATCH(*p)) {
					state_stack_pop(&tokeniser->state_stack);
					state_stack_push(&tokeniser->state_stack, TOKENISER_STATE(string));
				}
				else {
					token.type = TOKEN(unknown);
				}
				break;
			}
			case TOKENISER_STATE(single_line_comment): {
				if (*p == '\n') {
					state_stack_pop(&tokeniser->state_stack);
				}
				else {
					p++;
				}
				break;
			}
			case TOKENISER_STATE(multi_line_comment): {
				if (*p == '*') {
					state_stack_push(&tokeniser->state_stack, TOKENISER_STATE(multi_line_comment_end));
					p++;
				}
				else {
					p++;
				}
				break;
			}
			case TOKENISER_STATE(multi_line_comment_end): {
				if (*p == '/') {
					state_stack_pop(&tokeniser->state_stack);
					state_stack_pop(&tokeniser->state_stack);
					p++;
				}
				else {
					state_stack_pop(&tokeniser->state_stack);
					p++;
				}
				break;
			}
			default: {
				break;
			}
		}
	}

	switch (token.type) {
		case TOKEN(none): {
			if (0 != tokeniser->match_start) {
				if (0 != tokeniser->prev_match_storage) {
					fprintf(stderr, "Come on dude (Unimplemented but could be done by reallocating a larger buffer and copying incomplete data into it\n");
					abort();
				}

				size_t const curr_match_size = p - tokeniser->match_start;
				tokeniser->prev_match_storage = mem_for_string(tokeniser, curr_match_size + 1);
				memcpy(tokeniser->prev_match_storage, tokeniser->match_start, curr_match_size);
				tokeniser->prev_match_size = curr_match_size;
			}
			break;
		}
		case TOKEN(string): {
			char const * curr_match_data = (tokeniser->match_start ? tokeniser->match_start : *chunk_data);
			size_t curr_match_size = p - curr_match_data;
			char const * string_start = curr_match_data;

			size_t const total_size = curr_match_size + tokeniser->prev_match_size;
			assert(total_size + 1 <= MAX_STRING_LEN);

			char * string_mem = mem_for_string(tokeniser, total_size + 1);
			memcpy(string_mem, tokeniser->prev_match_storage, tokeniser->prev_match_size);
			memcpy(string_mem + tokeniser->prev_match_size, string_start, curr_match_size);
			string_mem[total_size] = '\0';

			// Adjust for quoted strings
			if (token.tags & xcproj_token_tag_string_is_literal) {
				string_mem++;
				string_mem[total_size - 2] = '\0';
			}

			token.string = string_mem;

			tokeniser->prev_match_storage = 0;
			tokeniser->prev_match_size = 0;
			break;
		}
		default: {
			break;
		}
	}

	// Reset match_start, we have stored away any previous partially read tokens
	tokeniser->match_start = 0;

	// Adjust input chunk markers to match what has been processed
	*chunk_size -= p - *chunk_data;
	*chunk_data = p;

	return token;
}

