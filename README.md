# README #

This is a small project I began as a little exercise in writing a parser from the ground up. I found the need to extract build settings and the like from large projects that made doing so time-consuming and hard to do with the Xcode interface. I figured it would actually be relatively straightforward to create something that could serialise/deserialise the Xcode project document format and work with this instead. The Xcode project document format is the old NeXT style plist format (see here: https://developer.apple.com/library/content/documentation/Cocoa/Conceptual/PropertyLists/OldStylePlists/OldStylePLists.html) 

### What does the project include? ###

The core of the project is the xcproj library, source under lib/xcproj. This has a C interface for tokenising and parsing the NeXT style plist. Designed to be fully re-entrant, with the idea being that you can pass whatever chunks of source text desired at a time, and it will inform you when the input is complete. Text reading/handling is to be performed by the user of this library.
On top of this I aim to build some higher level C++ interfaces for reading/writing specifically the xcode project format. This includes reading/handling/generating the 96-bit UUIDs used in the project for identifying everything, and the various project specific object types for easy reading and editing of build properties.

### How do I get set up? ###

Uses cmake for configuration.
This has only been compiled/tested on macOS/clang but at least the core library is straight C so should work elsewhere just fine.