#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "xcproj/parse/tokeniser.h"
#include "xcproj/parse/parser.h"


static void begin_obj_def(void * user_context) {
	printf("Begin object definition\n");
}

static void end_obj_def(void * user_context) {
	printf("End object definition\n");
}

static void begin_array_def(void * user_context) {
	printf("Begin array definition\n");
}

static void end_array_def(void * user_context) {
	printf("End array definition\n");
}

static void begin_assign(void * user_context, char const * identifier) {
	printf("Begin assignment to %s\n", identifier);
}

static void end_assign(void * user_context) {
	printf("End assignment\n");
}

static void handle_string_constant(void * user_context, char const * string) {
	printf("String constant: %s\n", string);
}


int main(int argc, char ** argv) {
	int err;
	char * input_block = 0;
	xcproj_tokeniser_t * tokeniser;
	xcproj_parser_t * parser;

	xcproj_parser_callback_container_t callbacks = {
		&begin_obj_def,
		&end_obj_def,
		&begin_array_def,
		&end_array_def,
		&begin_assign,
		&end_assign,
		&handle_string_constant
	};
	
	// Initialisation
	err = 0;
	tokeniser = xcproj_tokeniser_create();
	parser = xcproj_parser_create(callbacks);
	
	#define INPUT_BLOCK_SIZE	2048

	input_block = (char *)malloc(sizeof(char) * INPUT_BLOCK_SIZE);

	size_t count;
	while ((count = fread(input_block, sizeof(char), INPUT_BLOCK_SIZE, stdin)) > 0) {

		char const * data_ptr = input_block;
		size_t data_size = count;

		while (data_size > 0 && !err) {
			xcproj_token_t token = xcproj_tokeniser_next_token(tokeniser, &data_ptr, &data_size);

			// token type 'none' means the input data was finished
			if (xcproj_token_type_none == token.type)
				break;

			xcproj_parser_error_t result = xcproj_parser_parse_token(parser, &token, NULL);

			if (xcproj_parser_error_none != result) {
				fprintf(stderr, "ERROR: %s \n", xcproj_parser_error_get_description(result));
				break;
			}
		}
	}

	xcproj_parser_destroy(parser);
	xcproj_tokeniser_destroy(tokeniser);

	return err;
}

