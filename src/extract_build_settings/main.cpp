#include <iostream>
#include <fstream>
#include <cassert>
#include <string>
#include <vector>
#include <map>
#include <set>


#include "xcproj/document/document.hpp"


using namespace xcproj;

int main(int argc, char ** argv) {

	// Check we have at least one argument, the output folder
	if (argc < 2) {
		std::cerr << "Not enough arguments provided! Please provide the output folder as an argument." << std::endl;
		return 1;
	}

	// Parse the input and build the document object
	document input_document(std::cin);

	// Start traversing the tree
	document::node * curr_node = input_document.base_node;
	assert(curr_node->value.is<document::obj_node>());

	document::node * objects_node = nullptr;
	{
		document::obj_node & curr_obj_node = curr_node->value.get<document::obj_node>();

		auto iter = curr_obj_node.pairs.find("objects");

		assert(iter != curr_obj_node.pairs.end());

		objects_node = iter->second;
	}

	assert(objects_node != nullptr);

	document::obj_node & objects_obj_node = objects_node->value.get<document::obj_node>();

	// Gather target nodes
	document::node * project_node = nullptr;
	std::vector<document::node *> target_nodes{};
	{
		for (auto pair : objects_obj_node.pairs) {
			if (pair.second->value.is<document::obj_node>()) {
				document::obj_node & object_entry = pair.second->value.get<document::obj_node>();

				auto iter = object_entry.pairs.find("isa");

				if (iter != object_entry.pairs.end()) {
					assert(iter->second->value.is<std::string>());

					std::string & isa_string = iter->second->value.get<std::string>();

					if (isa_string == "PBXNativeTarget") {
						target_nodes.push_back(pair.second);
					}
					if (isa_string == "PBXProject") {
						project_node = pair.second;
					}
				}
			}
		}
	}

	assert(project_node != nullptr);
	assert(target_nodes.size() > 0);

	// Get the configuration lists for the project, these form the basis of the
	// configurations for each target
	std::map<std::string, document::node *> project_configurations;
	{
		assert(project_node->value.is<document::obj_node>());

		document::obj_node & project_obj_node = project_node->value.get<document::obj_node>();

		auto iter = project_obj_node.pairs.find("buildConfigurationList");
		assert(iter != project_obj_node.pairs.end());

		document::node * configuration_list_id_node = iter->second;
		assert(configuration_list_id_node->value.is<std::string>());

		std::string & configurationListID = configuration_list_id_node->value.get<std::string>();

		// Find the configuration list
		auto list_iter = objects_obj_node.pairs.find(configurationListID);
		assert(list_iter != objects_obj_node.pairs.end());

		document::node * configuration_list_node = list_iter->second;
		assert(configuration_list_node->value.is<document::obj_node>());
		document::obj_node & configuration_list_obj = configuration_list_node->value.get<document::obj_node>();

		auto configurations_iter = configuration_list_obj.pairs.find("buildConfigurations");
		assert(configurations_iter != configuration_list_obj.pairs.end());

		document::node * configurations_array_node = configurations_iter->second;
		assert(configurations_array_node->value.is<document::array_node>());

		document::array_node & configurations_array = configurations_array_node->value.get<document::array_node>();

		for (auto configuration_id : configurations_array.elements) {
			assert(configuration_id->value.is<std::string>());
			
			auto configuration_iter = objects_obj_node.pairs.find(configuration_id->value.get<std::string>());
			assert(configuration_iter != objects_obj_node.pairs.end());

			document::node * configuration_node = configuration_iter->second;
			assert(configuration_node->value.is<document::obj_node>());

			// Configuration name
			document::obj_node & configuration_obj = configuration_node->value.get<document::obj_node>();
			auto name_iter = configuration_obj.pairs.find("name");
			assert(name_iter != configuration_obj.pairs.end());

			document::node * configuration_name_node = name_iter->second;
			assert(configuration_name_node->value.is<std::string>());

			std::string & configuration_name = configuration_name_node->value.get<std::string>();

			std::cout << "Found configuration for project: " << configuration_name << '\n';

			project_configurations.emplace(configuration_name, configuration_node);
		}
	}


	// Loop through the target nodes and generate xconfig files for each
	for (auto target_node : target_nodes) {
		assert(target_node->value.is<document::obj_node>());

		document::obj_node & obj_node = target_node->value.get<document::obj_node>();

		// Name
		std::string name;
		{
			auto iter = obj_node.pairs.find("name");
			assert(iter != obj_node.pairs.end());
			
			document::node * name_node = iter->second;
			assert(name_node->value.is<std::string>());

			name = iter->second->value.get<std::string>();

			std::cout << "Found target: " << name << '\n';
		}

		assert(!name.empty());


		// Configuration list
		std::string buildConfigurationList;
		{
			auto iter = obj_node.pairs.find("buildConfigurationList");
			assert(iter != obj_node.pairs.end());

			document::node * configuration_list_node = iter->second;
			assert(configuration_list_node->value.is<std::string>());

			buildConfigurationList = configuration_list_node->value.get<std::string>();
		}

		assert(!buildConfigurationList.empty());

		// Configurations
		std::vector<document::node *> configurations;
		{
			auto iter = objects_obj_node.pairs.find(buildConfigurationList);
			assert(iter != objects_obj_node.pairs.end());

			document::node * list_node = iter->second;
			assert(list_node->value.is<document::obj_node>());

			document::obj_node & list_obj_node = list_node->value.get<document::obj_node>();

			// Get the array for the configuration id strings themselves
			std::vector<std::string> configurationIDs;
			{
				auto array_iter = list_obj_node.pairs.find("buildConfigurations");
				assert(iter != list_obj_node.pairs.end());

				document::node * configurations_node = array_iter->second;
				assert(configurations_node->value.is<document::array_node>());

				document::array_node & configurations_array_node = configurations_node->value.get<document::array_node>();

				for (auto configuration_node : configurations_array_node.elements) {	
					assert(configuration_node->value.is<std::string>());

					configurationIDs.push_back(configuration_node->value.get<std::string>());
				}
			}

			assert(configurationIDs.size() > 0);

			// Find the configuration nodes and add them to the list
			for (auto configurationID : configurationIDs) {
				auto configuration_iter = objects_obj_node.pairs.find(configurationID);
				assert(configuration_iter != objects_obj_node.pairs.end());

				configurations.push_back(configuration_iter->second);
			}
		}

		assert(configurations.size() > 0);



		// Sort the values as we'd want them
		// - per-configuration - set of pairs of settings -> value arrays
		// If setting is in all configurations, move to common
		// If no configuration specific setting, just straight up output
		// If configuration specific, output common + per-configuration, with final combination of setting

		// Oh dear lord
		using configuration_settings_values = std::map<std::string, int>;
		using configuration_settings_set = std::map<std::string, configuration_settings_values>;
		using configuration_settings_lookup = std::map<std::string, configuration_settings_set>;
		configuration_settings_lookup configuration_settings{};

		for (auto configuration : configurations) {
			assert(configuration->value.is<document::obj_node>());
			document::obj_node & configuration_obj = configuration->value.get<document::obj_node>();

			// Name
			std::string configuration_name;
			{
				auto iter = configuration_obj.pairs.find("name");
				assert(iter != configuration_obj.pairs.end());

				document::node * name_node = iter->second;
				assert(name_node->value.is<std::string>());

				configuration_name = name_node->value.get<std::string>();

				std::cout << "Found configuration for target " << name << ": " << configuration_name << '\n';
			}

			// Create entry if doesn't exist, get settings bag for this configuration
			configuration_settings_set & this_configuration_settings = configuration_settings[configuration_name];

			// Actual configuration settings
			{
				auto settings_iter = configuration_obj.pairs.find("buildSettings");
				assert(settings_iter != configuration_obj.pairs.end());

				document::node * configuration_settings_node = settings_iter->second;
				assert(configuration_settings_node->value.is<document::obj_node>());
				
				document::obj_node & configuration_settings_obj = configuration_settings_node->value.get<document::obj_node>();
				
				for (auto setting : configuration_settings_obj.pairs) {

					std::vector<document::node *> values{};
					if (setting.second->value.is<document::array_node>()) {
						document::array_node & settings_array = setting.second->value.get<document::array_node>();
						values = settings_array.elements;
					}
					else {
						values.push_back(setting.second);
					}

					for (auto val : values) {
						if (val->value.is<std::string>()) {
							this_configuration_settings[setting.first][val->value.get<std::string>()];
						}
						else if (val->value.is<int>()) {
							this_configuration_settings[setting.first][std::to_string(val->value.get<int>())];
						}
					}
				}
			}

			// Is there a project-level configuration node for this configuration?
			// If there is, get these settings first, as they form the basis for target
			// specific settings
			auto project_configuration_iter = project_configurations.find(configuration_name);
			if (project_configuration_iter != project_configurations.end()) {
				document::node * configuration_node = project_configuration_iter->second;
				assert(configuration_node->value.is<document::obj_node>());

				document::obj_node & configuration_obj = configuration_node->value.get<document::obj_node>();

				auto settings_iter = configuration_obj.pairs.find("buildSettings");
				assert(settings_iter != configuration_obj.pairs.end());

				document::node * configuration_settings_node = settings_iter->second;
				assert(configuration_settings_node->value.is<document::obj_node>());
				
				document::obj_node & configuration_settings_obj = configuration_settings_node->value.get<document::obj_node>();
				
				for (auto setting : configuration_settings_obj.pairs) {

					// Only set these if there was no per-target configuration value for this setting
					if (this_configuration_settings.find(setting.first) != this_configuration_settings.end()) {
						continue;
					}

					std::vector<document::node *> values{};
					if (setting.second->value.is<document::array_node>()) {
						document::array_node & settings_array = setting.second->value.get<document::array_node>();
						values = settings_array.elements;
					}
					else {
						values.push_back(setting.second);
					}

					for (auto val : values) {
						if (val->value.is<std::string>()) {
							this_configuration_settings[setting.first][val->value.get<std::string>()] = 1;
						}
						else if (val->value.is<int>()) {
							this_configuration_settings[setting.first][std::to_string(val->value.get<int>())] = 1;
						}
					}
				}
			}
		}

		// Gather common settings
		configuration_settings_set common_settings{};
		auto & first_configuration_settings = configuration_settings.begin()->second;
		for (auto & setting : first_configuration_settings) {

			for (auto val_iter = setting.second.begin();
				val_iter != setting.second.end();
				) {

				bool is_common = true;

				// See if the value is common for this setting in all configurations
				for (auto config_iter = ++configuration_settings.begin();
					config_iter != configuration_settings.end();
					++config_iter) {

					configuration_settings_set & curr_configuration_settings = config_iter->second;

					if (!curr_configuration_settings[setting.first].count(val_iter->first)) {
						is_common = false;
						break;
					}
				}


				// If it is, remove it from configuration-specific settings
				// and put into common
				if (is_common) {
					for (auto config_iter = ++configuration_settings.begin();
						config_iter != configuration_settings.end();
						++config_iter) {

						configuration_settings_set & curr_configuration_settings = config_iter->second;

						configuration_settings_values & curr_values = curr_configuration_settings[setting.first];
						auto curr_val_iter = curr_values.find(val_iter->first);

						if (curr_val_iter != curr_values.end()) {
							curr_values.erase(curr_val_iter);
						}
					}

					// Make a copy in common_settings
					common_settings[setting.first][val_iter->first] = val_iter->second;

					val_iter = setting.second.erase(val_iter);
				}
				else {
					++val_iter;
				}
			}
		}

		// Open an xcconfig up for output specifically for this target in the output directory
		std::string target_xcconfig_path(argv[1]);
		target_xcconfig_path += '/';
		target_xcconfig_path += name;
		target_xcconfig_path += ".xcconfig";
		std::ofstream xcconfig_output_stream(target_xcconfig_path.c_str(), std::ofstream::out);

		xcconfig_output_stream << "//========================================\n";
		xcconfig_output_stream << "//\t\t" << name << " Settings\n";
		xcconfig_output_stream << "//----------------------------------------\n\n\n";

		
		std::map<std::string, int> seen_settings{};
		size_t longest_seen_setting_name_len = 0;
		size_t longest_seen_configuration_name_len = 0;
		for (auto this_configuration : configuration_settings) {
			if (this_configuration.first.size() > longest_seen_configuration_name_len) {
				longest_seen_configuration_name_len = this_configuration.first.size();
			}

			for (auto setting : this_configuration.second) {
				seen_settings[setting.first] = 1;
				if (setting.first.size() > longest_seen_setting_name_len) {
					longest_seen_setting_name_len = setting.first.size();
				}
			}
		}

		size_t const longest_total_name_len = longest_seen_setting_name_len + longest_seen_configuration_name_len + 1;

		std::map<std::string, int> combine_settings{};
		for (auto this_configuration_settings : configuration_settings) {
			std::string const & configuration_name = this_configuration_settings.first;

			xcconfig_output_stream << "//----------------------------------------\n";
			xcconfig_output_stream << "//\t\t" << configuration_name << '\n';
			xcconfig_output_stream << "//----------------------------------------\n";


			for (auto setting : this_configuration_settings.second) {
				if (setting.second.size() > 0) {
					combine_settings[setting.first] = 1;
					xcconfig_output_stream << setting.first << '_' << configuration_name;
					
					size_t setting_name_len_diff = longest_total_name_len - setting.first.size() - configuration_name.size() - 1;
					while (setting_name_len_diff --> 0) {
						xcconfig_output_stream << ' ';
					}

					xcconfig_output_stream << '=';

					for (auto val : setting.second) {
						xcconfig_output_stream << ' ' << val.first; 
					}

					xcconfig_output_stream << '\n';
				}
			}

			xcconfig_output_stream << "\n\n\n\n\n";
		}

		xcconfig_output_stream << "//========================================\n";
		xcconfig_output_stream << "//\t\tCombined settings\n";
		xcconfig_output_stream << "//----------------------------------------\n";

		for (auto setting : seen_settings) {
			xcconfig_output_stream << setting.first;

			size_t setting_name_len_diff = longest_total_name_len - setting.first.size();
			while (setting_name_len_diff --> 0) {
				xcconfig_output_stream << ' ';
			}

			xcconfig_output_stream << '=';

			if (combine_settings[setting.first]) {
				xcconfig_output_stream << " $(" << setting.first << "_$(CONFIGURATION))";
			}

			auto & common_setting = common_settings[setting.first];
			for (auto val : common_setting) {
				xcconfig_output_stream << ' ' << val.first;
			}

			xcconfig_output_stream << '\n';
		}
	}

	return 0;
}

