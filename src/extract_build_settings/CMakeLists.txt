cmake_minimum_required(VERSION 2.8)
project(extract_build_settings)

set(CMAKE_CXX_FLAGS_DEBUG
	"${CMAKE_CXX_FLAGS_DEBUG} -O0 -fsanitize=address -fno-omit-frame-pointer"
)

set(SRCS
	${CMAKE_CURRENT_SOURCE_DIR}/main.cpp
)

add_executable(extract_build_settings
	${SRCS}
)

target_link_libraries(extract_build_settings
	xcproj
)

set_property(TARGET extract_build_settings PROPERTY CXX_STANDARD 11)
set_property(TARGET extract_build_settings PROPERTY CXX_STANDARD_REQUIRED ON)

install(TARGETS extract_build_settings
	RUNTIME DESTINATION bin
)

