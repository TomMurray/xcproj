#include "xcproj/document/document.hpp"

#include <iostream>

int main(int argc, char ** argv) {

	xcproj::document new_document;

	new_document.from_stream(std::cin);

	// Write it back out to see what we got
	new_document.to_stream(std::cout);
	
	return 0;
}
